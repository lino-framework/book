import datetime

from ..settings import *


class Site(Site):
    is_demo_site = True
    the_demo_date = datetime.date(2017, 3, 12)

    # ignore_dates_after = datetime.date(2019, 05, 22)

    def get_installed_plugins(self):
        yield super().get_installed_plugins()
        yield 'lino_xl.lib.b2c'


SITE = Site(globals())
DEBUG = True
SITE.plugins.periods.configure(start_year=2016)
