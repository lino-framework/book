# -*- coding: UTF-8 -*-
# Copyright 2015-2017 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""A copy of the :ref:`noi` as a ticket management demo, for testing Django migrations.

.. autosummary::
   :toctree:

   settings

"""
