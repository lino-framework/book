# -*- coding: UTF-8 -*-
# Copyright 2017 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Shows :ref:`tera`, a management tool for therapeutic centres.

.. autosummary::
   :toctree:

   settings

"""
