"""
An example of a :ref:`cosi` configured for usage in Chinese.

The only differences are some plugin configuration settings.

This is not being used except for testing translation.

.. autosummary::
   :toctree:

    settings

"""
