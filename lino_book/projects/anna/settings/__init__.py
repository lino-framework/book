# -*- coding: UTF-8 -*-
# Copyright 2014-2017 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""

.. autosummary::
   :toctree:

   demo
   doctests
   www
   memory



"""

# from lino_book.projects.noi1e.settings import *
from lino_care.lib.care.settings import *
from lino.api.ad import _
