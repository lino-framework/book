# -*- coding: UTF-8 -*-
# Copyright 2013-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
A Lino Voga variant for Roger.
Adds some very specific local customizations.

This demo project is used by the following tested docs:
:doc:`/specs/checkdata`,
:doc:`/plugins/invoicing`,
:doc:`/specs/voga/print_labels`,
:doc:`/specs/voga/checkdata`,
:doc:`/specs/voga/invoicing`,
:doc:`/specs/voga/roger`,
:doc:`/specs/voga/voga`,
:doc:`/specs/voga/pupils`,
:doc:`/specs/voga/holidays`,
:doc:`/specs/voga/partners`,
:doc:`/specs/voga/usertypes`,
:doc:`/specs/voga/cal`,
:doc:`/specs/voga/trading`,
:doc:`/specs/voga/courses`,
:doc:`/specs/voga/accounting`,
:doc:`/specs/voga/presences`,
:doc:`/specs/weasyprint`,


"""
