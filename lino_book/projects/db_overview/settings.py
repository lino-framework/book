from lino.projects.std.settings import *

SITE = Site(globals(), 'lino_book.projects.db_overview')
SITE.site_locale = "en_US.utf-8"

DEBUG = True
