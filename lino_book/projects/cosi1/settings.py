# -*- coding: UTF-8 -*-
# Copyright 2014-2025 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Default settings for a :ref:`cosi` "à la APC".

"""

from lino_cosi.lib.cosi.settings import *
from lino_xl.lib.ibanity.utils import DEMO_SUPPLIER_ID


class Site(Site):
    demo_fixtures = 'std few_countries minimal_ledger \
    furniture \
    demo demo_bookings payments demo2 demo3 checkdata'.split()

    project_name = 'cosi1'
    is_demo_site = True
    # ignore_dates_after = datetime.date(2019, 5, 22)
    the_demo_date = 20150312
    use_help = True
    languages = 'de fr en'
    # use_ipdict = True

    default_ui = "lino_react.react"
    user_types_module = "lino_book.projects.cosi1.user_types"
    # use_peppol = True

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        yield ('vat', 'declaration_plugin', 'lino_xl.lib.bevat')
        yield ('countries', 'hide_region', True)
        yield ('countries', 'country_code', 'BE')
        yield ('excerpts', 'responsible_user', 'robin')
        yield ('accounting', 'use_pcmn', True)
        yield ('accounting', 'worker_model', 'contacts.Person')
        yield ('periods', 'start_year', 2014)
        # yield ('users', 'active_sessions_limit', 1)
        yield ("ibanity", "supplier_id", DEMO_SUPPLIER_ID)
        # yield ("ibanity", "inbound_journal", "INB")
        # yield ("ibanity", "outbound_model", "trading.VatProductInvoice")
        # yield ("ibanity", "inbound_model", "vat.VatAccountInvoice")


SITE = Site(globals())

DEBUG = True
