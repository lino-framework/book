from lino_book.projects.estref.settings import *

SITE = Site(globals(), title=Site.verbose_name + " demo", is_demo_site=True)
DEBUG=True
