.. _dg.topics.peppol:

===================================
Peppol support (eInvoicing) in Lino
===================================

Lino supports receiving and sending digital invoices via the :term:`Peppol
network` as `required by companies in Belgium from January 2026
<https://finances.belgium.be/fr/entreprises/tva/e-facturation/utilisation-obligatoire-factures-electroniques-structurees-a-partir-de-2026#:~>`__.

This document explain a few general things about Peppol in our words.
Implementation details are in :doc:`/plugins/ibanity/index`.


..
  As a Lino :term:`site operator` you need a contract with a :term:`hosting
  provider` who has a contract with :term:`Ibanity`. Your :term:`hosting provider`
  will register you as an :term:`Ibanity` end user and :term:`Ibanity` will act as
  your Peppol Access Point. We are considering other access point providers as
  well, for example `soluz.io <https://www.soluz.io/general-9>`__.



.. contents::
   :depth: 1
   :local:


.. include:: /../docs/shared/include/tested.rst

>>> from lino_book.projects.cosi1.startup import *



.. What's implemented

.. Choosing your Access Point provider

..
  Every Lino :term:`site operator` who wants Peppol access must make a contract
  with some AP provider to get credentials for accessing their API.


..
  We are considering other implementation as well, for example `Tickstart
  <https://www.tickstar.com/>`_. There are many `certified AP providers
  <https://peppol.org/members/peppol-certified-service-providers/>`__, but some
  of them cannot help because they provide access only via their own accounting
  software.


..
  Lino can generate electronic sales invoices compliant with the `European
  standard on eInvoicing
  <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eInvoicing>`__.

  To create the XML of an electronic sales invoice, simply print the invoice. This
  will generate an :term:`XML file` that follows the EN 16931-1 standard
  (:term:`Peppol` format). The printable PDF file contains a link to the generated
  :term:`XML file`.

  Note that invoices to private persons don't generate any :term:`XML file`
  because :term:`Peppol` is currently meant for B2B exchanges.

  Try our demo

  If you want to see it with your own eyes:

  - Go to https://cosi1e.lino-framework.org and sign in as ``robin``

  - Click on the link "730 Sales invoices (SLS)" in the main page

  - Select an invoice having a company (not a private person) as partner. For
    example invoice SLS 26/2024.

  - Click on the :guilabel:`Print` button in the toolbar. If needed, tell your
    browser to accept pop-up windows from this site.

  - When the pdf opens, click on `e-invoice: /media/xml/SLS/1787.xml
    <https://cosi1e.lino-framework.org/media/xml/SLS/1815.xml>`__ to see the XML
    file.


  There is more to do

  - Transmission of the XML file to an access point is not yet implemented.

  - Import Peppol invoice files.

  - The ecosio validator still reports issues.

.. Here is some :term:`UBL` jargon and how it maps to general Peppol jargon.

Generating Peppol XML files
===========================

Lino has two database models that represent a :term:`Peppol document`:
:class:`VatProductInvoice <lino_xl.lib.trading.VatProductInvoice>` and
:class:`VatAccountInvoice <lino_xl.lib.vat.VatAccountInvoice>`. The former is
usually used for sales and the latter for purchase.

>>> for m in rt.models_by_base(jinja.XMLMaker):
...     if m.xml_file_template:
...         print(full_model_name(m), m.xml_file_template, m.xml_validator_file)
... #doctest: +ELLIPSIS
trading.VatProductInvoice vat/peppol-ubl.xml .../lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch
vat.VatAccountInvoice vat/peppol-ubl.xml .../lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch

In order to create the XML of a :term:`Peppol document`, Lino simply parses the
:xfile:`vat/peppol-ubl.xml` template.

Lino currrently does not validate Peppol documents. See :doc:`schematron`.

You can manually validate the generated XML file for example on `ecosio.org
<https://ecosio.com/en/peppol-and-xml-document-validator/>`__.



Peppol code lists
=================

There is a register of `OpenPeppol eDEC Code Lists
<https://docs.peppol.eu/edelivery/codelists/>`__


Electronic Address Scheme
=========================

One of the challenges of Peppol was how to identify your business partners
across national borders. Peppol suggests a standard for international
identification of legal persons.

Peppol does this in two steps: first you specify one of the recognized official
registries of identification codes. Peppol knows a few hundred of them.  These
are called "Electronic Address Schemes"
or . Let's call them :term:`EAS`.

.. glossary::

  EAS

    Abbreviation for "Electronic Address Scheme".
    A recognized official registry of identification codes.

    Some documents call them "Participant Identifier Schemes": `example
    <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/Registry+of+supporting+artefacts+to+implement+EN16931?preview=/467108974/691830878/Electronic%20Address%20Scheme%20Code%20list%20-%20version%2012%20-%20published%20Sept2023.xlsx>`__)

Some examples of :term:`EAS` codes:

- 9925 : Belgium VAT number
- 0208 : Belgium Company number
- 9931 : Estonian VAT number

It seems that EAS 9925 is a subset of EAS 0208: every party subject to VAT in
Belgium also as a company number, which is the same as its VAT number. Hence
normal companies should be in both registers, while companies like insurances
and some non-profit organizations (who are not subject to VAT and hence have no
VAT number) will be only in EAS 0208.

The `commondata.peppolcodes
<https://github.com/lsaffre/commondata/tree/master?tab=readme-ov-file#peppol-codes>`__
module defines a dict :data:`COUNTRY2SCHEME`,
which maps country codes to the :term:`EAS` number of the VAT office of that
country. Lino uses this when generating a Peppol XML file for outbound
documents.

>>> from commondata.peppolcodes import COUNTRY2SCHEME

>>> peppol_countries = set(COUNTRY2SCHEME.keys())

The list contains some countries that are not part of the European Union
(according to :data:`lino_xl.lib.vat.eu_country_codes`):

>>> sorted(peppol_countries - dd.plugins.vat.eu_country_codes)
['AD', 'AL', 'BA', 'CH', 'GB', 'LI', 'MC', 'ME', 'MK', 'NO', 'RS', 'SM', 'TR', 'VA', 'international']

TODO: Denmark is currently *not* in our list because it doesn't have a registry
scheme that ends with "VAT".

>>> dd.plugins.vat.eu_country_codes - peppol_countries
{'DK'}


The parties of an invoice
=========================

- Seller : the partner who sells. Also called supplier, provider.
  See `cac:AccountingSupplierParty <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-AccountingSupplierParty/>`__

- Buyer : the partner who buys. Also called invoicee, recipient, customer.
  See `cac:AccountingCustomerParty <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-AccountingCustomerParty/>`__

- Payee : the partner who receives the payment. Shall be used when the Payee is
  different from the Seller. We currently don't use this element	``<cac:PayeeParty>``.

- Tax representative : ``<cac:TaxRepresentativeParty>`` : not mandatory and we
  don't know what it is used for.


Both the seller and the buyer of an invoice contain a single mandatory element
`cac:Party
<https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-AccountingSupplierParty/cac-Party/>`__,
which contains a mandatory element `cbc:EndpointID
<https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-AccountingSupplierParty/cac-Party/cbc-EndpointID/>`__,
which identifies the party's electronic address.

This ``EndpointID`` element contains the identification number as a textual
value, and must have an attribute ``schemeID``, which refers to the :term:`EAS`
code to use when looking up the identification number.


.. _dg.topics.peppol.vatcat:

The VAT category
================

The ``<cac:ClassifiedTaxCategory>`` element contains "a group of business terms
providing information about the VAT applicable for the goods and services
invoiced on the invoice or the invoice line."

- ``<cbc:ID>`` : The :term:`VAT category` code for the invoiced item.

- ``<cbc:Percent>`` : The applied VAT rate. A number, potentially with decimal
  positions

- ``<cac:TaxScheme>`` : a mysterious but mandatory element. According to our
  references it must contain exactly one child element ``<cbc:ID>VAT</cbc:ID>``,
  where the word "VAT" seems to mean that the seller is identified using their
  VAT identifier. We don't know whether it may contain other values.

A good introduction into why we have all these categories and rates is here:
`VAT Rates in Europe 2024
<https://www.globalvatcompliance.com/globalvatnews/vat-rates-in-europe-2021/>`__


.. glossary::

  VAT category

    An alphabetic code defined as part of Peppol in the `UNCL5305 code list
    <https://docs.peppol.eu/pracc/catalogue/1.0/codelist/UNCL5305/>`__

    This list specifies the allowed VAT categories:

    == =================== ===================================================
    AE Vat Reverse Charge  VAT is levied from the buyer.
    E  Exempt from Tax     Taxes are not applicable.
    S  Standard rate       The standard rate is applicable.
    Z  Zero rated goods    The goods are at a zero rate.
    H  Higher rate         A higher rate of duty or tax or fee is applicable.
    AA Lower rate          Tax rate is lower than standard rate.
    == =================== ===================================================

Two functions are used in the :xfile:`vat/peppol-ubl.xml` template:

- :meth:`VatItemBase.get_peppol_vat_category` returns the VAT category of a
  :term:`voucher item` by interpreting the :term:`VAT regime` and :term:`VAT
  class`. This is used for the ``<cac:ClassifiedTaxCategory>`` element.

- :meth:`linox_xl.lib.VatVoucher.get_vat_subtotals` returns an iterator of
  4-tuples `(categ, rate, total_base, total_vat)`. These are used for the
  ``<cac:TaxSubtotal>`` elements of the voucher.


>>> dd.plugins.vat.declaration_plugin
'lino_xl.lib.bevat'

>>> rows = []
>>> coll = collections.OrderedDict()
>>> for obj in trading.InvoiceItem.objects.all():
...     k = (obj.vat_class, obj.voucher.vat_regime, obj.get_peppol_vat_category())
...     if k not in coll:
...         coll[k] = obj
>>> for k, obj in coll.items():
...     vat_class, vat_regime, pc = k
...     rows.append([vat_class.name, vat_regime.name, pc, obj])
>>> headers = ["Class", "Regime", "Cat.", "Line"]
>>> print(rstgen.table(headers, rows))
========== ========== ====== ===============
 Class      Regime     Cat.   Line
---------- ---------- ------ ---------------
 services   subject    S      SLS 1/2014#1
 services   intracom   AE     SLS 2/2014#1
 reduced    subject    AA     SLS 6/2014#1
 exempt     subject    Z      SLS 6/2014#2
 reduced    intracom   AE     SLS 11/2014#2
 exempt     intracom   AE     SLS 12/2014#1
 services   normal     S      SLS 14/2014#1
 reduced    normal     AA     SLS 17/2014#1
 exempt     normal     Z      SLS 17/2014#2
========== ========== ====== ===============
<BLANKLINE>

>>> obj = trading.VatProductInvoice.objects.get(pk=111)
>>> print(f"{obj} {obj.total_base} {obj.total_vat} {obj.total_incl}")
SLS 6/2014 1110.16 203.87 1314.03
>>> round(1110.16 + 203.87, 2)
1314.03
>>> headers = ['Cat', "Rate", "Base", "VAT"]
>>> rows = []
>>> for cat, rate, total_base, total_vat in obj.get_vat_subtotals():
...     rows.append([cat, rate, total_base, total_vat])
>>> print(rstgen.table(headers, rows))
===== ====== ======== ========
 Cat   Rate   Base     VAT
----- ------ -------- --------
 AA    0.12   299.00   35.88
 Z     0      11.20    0.00
 S     0.21   799.96   167.99
===== ====== ======== ========
<BLANKLINE>

>>> round(299 + 11.20 + 799.96, 2)
1110.16
>>> round(35.88 + 0 + 167.99, 2)
203.87

Some error messages and how we fix them
=======================================

- Error message: A buyer reference or purchase order reference MUST be provided.

  The specs about `cbc:BuyerReference
  <https://docs.peppol.eu/poacc/billing/3.0/2024-Q2/syntax/ubl-invoice/cbc-BuyerReference/>`_
  say indeed that "An invoice must have buyer reference or purchase order
  reference (BT-13)."

  🡒 When :attr:`your_ref` is empty, Lino writes "not specified".



Amounts
=======

Vocabulary:

- Line net amount: Invoiced quantity * Unit Gross Price
- Allowances : discount or similar amount to *subtract* from the net amount
- Charges : some fee, tax (other than VAT) or similar amount to *add* to the net
  amount
- Line extension amount : Net amount + Charges - Allowances.

An invoice must have exactly one `cac:LegalMonetaryTotal
<https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/>`__
element, which provides the monetary totals for the invoice. It can have the
following children (each child at most once):

- `cbc:LineExtensionAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-LineExtensionAmount/>`__:
  Sum of all invoice line amounts in the invoice, net of tax and settlement
  discounts, but inclusive of any applicable rounding amount.

- `cbc:TaxExclusiveAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-TaxExclusiveAmount/>`__:
  total amount of the invoice without VAT.

- `cbc:TaxInclusiveAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-TaxInclusiveAmount/>`__:
  total amount of the invoice with VAT.

- `cbc:AllowanceTotalAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-AllowanceTotalAmount/>`__

- `cbc:ChargeTotalAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-ChargeTotalAmount/>`__:
  Sum of all charges in the invoice.

- `cbc:PrepaidAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-PrepaidAmount/>`__:
  Sum of amounts that have been paid in advance.

- `cbc:PayableRoundingAmount <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-PayableRoundingAmount/>`__
  Amount to be added to the invoice total to round the amount to be paid.

- 'cbc:PayableAmount
  <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/cac-LegalMonetaryTotal/cbc-PayableAmount/>`__:
  outstanding amount that is requested to be paid


Amounts must be rounded to maximum 2 decimals.
Each amount element has a mandatory attribute ``currencyID``.


Sources of information
======================

ec.europa.eu (European Commission):

- `Transmitting electronic invoices <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/Transmitting+electronic+invoices>`__
- `eDelivery AS4 specification v1.15 <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eDelivery+AS4+-+1.15>`__
- `Code lists <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/Code+lists>`__

peppol.eu (Copyright OpenPeppol AISBL):

- `OpenPeppol eDEC Specifications <https://docs.peppol.eu/edelivery/>`__
  lists the specifications of the Peppol eDelivery network.

peppol.org  (Copyright OpenPeppol AISBL):

- https://peppol.org/documentation/technical-documentation/edelivery-documentation/

other:

- https://github.com/OpenPEPPOL/peppol-bis-invoice-3/blob/master/rules/examples/base-example.xml

- `Free PEPPOL and XML document validator <https://ecosio.com/en/peppol-and-xml-document-validator/>`__

- 2020-06-01 `The European Commission updates the Electronic Address Scheme (EAS) code lists
  <https://lmtgroup.eu/the-european-commission-updates-the-electronic-address-scheme-eas-code-lists/>`__

- https://bosa.belgium.be/fr/applications/hermes

My own blog entries:

- https://luc.lino-framework.org/blog/2024/0624.html
- https://luc.lino-framework.org/blog/2024/0704.html
- https://luc.lino-framework.org/blog/2024/1219.html (ff.)
- https://luc.lino-framework.org/blog/2025/0113.html (ff.)
- etc.


Peppol jargon
=============

Here are some Peppol-related terms and abbreviations and what they mean.

.. glossary::

  Peppol

    An international standard for exchanging structured electronic invoices and
    other business documents.

    https://docs.peppol.eu/poacc/billing/3.0/

  Peppol document

    A business document that can be exchanged via the :term:`Peppol network`.
    Right now this means an *invoice* or *credit note*, either *sales* or
    *purchase*.

    See also `Generating Peppol XML files`_.

  Peppol network

    The network of organizations who use the :term:`Peppol` standard for
    doing their business communication.

  Access Point

    An organization that provides connection to the :term:`Peppol network`.
    Abbreviated AP.

  UBL

    Universal Business Language

    `UBL Invoice Syntax reference <https://docs.peppol.eu/poacc/billing/3.0/syntax/ubl-invoice/tree>`__

  BIS

    Business Interoperability Specifications

    See https://docs.peppol.eu/poacc/billing/3.0/bis/


  SML

    Service Metadata Locator

    https://docs.peppol.eu/edelivery/sml/PEPPOL-EDN-Service-Metadata-Locator-1.2.0-2021-05-13.pdf

  SMP

    Service Metadata Publishing

  AS4

    The only transport profile still in use.

    "The AS4 technical specification [AS4] defines a secure and reliable
    messaging protocol. It can be used for message exchange in
    Business-to-Business (B2B), Administration-to-Administration (A2A),
    Administration-to-Business (A2B) and Business-to-Administration (B2A)
    contexts. AS4 messages can carry any number of payloads. Payloads may be
    structured or unstructured documents or data." (`ec.europa.eu
    <https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eDelivery+AS4+-+2.0>`__)

  Hermes

    A platform provided by the Belgian government where SMEs can sign in and
    communicate with the Peppol network. They can manually enter outbound
    documents (sales invoices and credit notes) to be sent to their customers,
    and they can view their inbound documents.

    - https://hermes-belgium.be
    - https://bosa.belgium.be/fr/applications/hermes
