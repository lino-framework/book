.. _writedocs:

=====================
Writing documentation
=====================

"Documentation is one of the foundational blocks for building a community."
"Think of writing docs as writing code."
-- Olga Merkulova in
`Write documentation that actually works for your community
<https://opensource.com/article/23/3/community-documentation>`__

We try to follow the `Diátaxis <https://diataxis.fr/>`__ principles. Diátaxis
identifies four modes of documentation: tutorials, how-to guides, technical
reference and explanation. Each of these modes (or types) answers to a different
user need, fulfills a different purpose and requires a different approach to its
creation.

.. toctree::
   :maxdepth: 1

   /dev/sphinx/intro
   /dev/builddocs
   /dev/doctests
   /team/doctrees
   /dev/devblog
   /dev/docstrings
   /dev/help_texts
   /dev/specs
   /dev/shared
   /dev/changelogs
   /dev/redirected
   shared
   makedocs
   intersphinx
