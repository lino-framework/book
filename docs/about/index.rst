.. _book.lino.about:

===============
More about Lino
===============

This section contains miscellaneous background information about Lino for developers.


.. toctree::
   :maxdepth: 1

   /dev/about/features
   /dev/about/lino_and_django
   /about/compared
   /dev/about/faq
   /about/overview
   /dev/about/why
   /dev/about/auth

   /dev/about/not_easy
   /dev/about/think_python
   /dev/about/ui
   /dev/warning
   /stats
   /apps
