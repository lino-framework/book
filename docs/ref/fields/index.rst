===========
Field types
===========


.. glossary::

  htmlbox

    A :term:`virtual field` (:class:`VirtualField`) of type
    :class:`lino.core.fields.HtmlBox`.
