.. _dg.commands:

=====================
Django-admin commands
=====================

.. toctree::
   :maxdepth: 1

   buildcache
   checkdata
   dump2py
   initdb
   prep
   run
