.. _specs:
.. _book.specs:

===================
Reference
===================

..
  This section of the :term:`developer documentation`
  is an entry point via structural entities or component.
  If you know the "key", then here is where you can look it up to get your answers.

.. toctree::
   :maxdepth: 1

   /changes/index
   /genindex
   /plugins/index
   /projects/index
   /apps/index
   commands/index
   settings
   /specs/synodal/index
   API </api/index>
   /about/overview

Miscellaneous reference pages
=============================

.. toctree::
   :maxdepth: 1

   demo_fixtures
   glossary
   fields/index
   links
   files
   tools
   javascript
   jargon
   obsolete

   /about/thanks
   /dev/newbies/index
