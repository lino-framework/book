===============================================
The ``lino.api.ad`` module (application design)
===============================================

.. module:: lino.api.ad




.. toctree::
    :maxdepth: 1

    plugin
    site



.. function:: _

   Marks a translatable text.

.. function:: pgettext

   Marks a translatable text with explicit context.
