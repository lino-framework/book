.. _book.changes:

=========
Changelog
=========

This is the combined changelog for packages :mod:`lino`, :mod:`lino_xl` and
:mod:`lino_react` and :mod:`commondata`.

General information about how to read and maintain this document in
:ref:`dev.changes`. Make sure to notify us if you run a :term:`production site`
using Lino.  We provide automatic database migration only for applications with
at least one registered production site.


.. toctree::
   :maxdepth: 1

   2025
   2024
   2023
   2022
   2021
   2020
   2019
   2018
   old
