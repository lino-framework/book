.. include:: /../docs/shared/include/defs.rst
.. _book.changes.2025:

====
2025
====

This is the 2025 :term:`change log` for Lino.
Learn how to read and maintain this document in :ref:`dev.changes`.

2025-03-06
==========

The :mod:`lino_xl.lib.accounting` plugin now defines an
:class:`lino.modlib.uploads.Shortcuts` :attr:`source_document`, which is used by
:mod:`lino_xl.lib.ibanity` when it finds an embedded pdf file in an inbound
:term:`Peppol document`.
As a side effect we optimized the rendering of upload shortcut fields.

2025-03-05
==========

.. currentmodule:: lino_xl.lib.ibanity

The :mod:`lino_xl.lib.ibanity` plugin gets ready for the integration phase. The
tables :class:`Inbox`, :class:`Archive`, :class:`Outbox` and :class:`Sent` are
now in the menu.  It has a :term:`quick link` to the :term:`system task` that
starts the synchronization. The :manage:`linod` daemon now runs as the user
specified by :attr:`lino.modlib.linod.daemon_user` instead of anonymously.
Internal optimization: :meth:`lino.core.kernel.Kernel.run_action` no longer
treats ValidationError and IntegrityError with a traceback.



2025-03-01
==========

.. currentmodule:: lino.modlib.printing

The method :meth:`BuildMethod.before_printable_build` has been replaced by
:meth:`BuildMethod.must_build_printable` is is now expected to return a boolean.
The :meth:`filename_root` method was renamed to
:meth:`get_printable_target_stem`. The :meth:`get_target_name` method was
removed, we now call ``self.get_target_file().name`` or
``self.get_target_file().path`` :class:`lino.utils.media.MediaFile` is now
implemented differently and has a :attr:`path` attribute instead of a
:attr:`name` property.

After upgrading a production server you should test whether editable print
actions still work.


2025-02-27
==========

Advancing with :ticket:`5670` (Support e-invoices using PEPPOL):  The values of
the individual choices  of :class:`lino_xl.lib.products.DeliveryUnits` are now
the codes that are allowed in the `unitCode
<https://docs.peppol.eu/poacc/billing/3.0/2024-Q2/syntax/ubl-invoice/cac-InvoiceLine/cbc-InvoicedQuantity/unitCode/>`__
attribute of a  `InvoicedQuantity
<https://docs.peppol.eu/poacc/billing/3.0/2024-Q2/syntax/ubl-invoice/cac-InvoiceLine/cbc-InvoicedQuantity/>`__
element. Added a new attribute :attr:`lino_xl.lib.vat.VatRegime.reverse_charge`,
which is `True` for the :term:`VAT regimes <VAT regime>` ``intracom`` and
``cocontractor``. This is currently used only by
:meth:`vat.VatItemBase.get_peppol_vat_category`. When migrating your database,
you can specify a :attr:`lino.core.ChoiceList.old2new` for :class:`DeliveryUnits
<lino_xl.lib.products.DeliveryUnits>` by adding the following line to your
:xfile:`restore.py` file::

    settings.SITE.models.products.DeliveryUnits.old2new = {"10": "HUR", "20": "XPP", "30". "KGM", "40": "XBX"}



2025-02-24
==========

Advancing with :ticket:`5670` (Support e-invoices using PEPPOL).
:attr:`lino_xl.lib.contacts.Partner.is_outbound`

2025-02-12
==========

.. currentmodule:: lino_xl.lib.invoicing

Fixed :ticket:`5924` (Menu "My invoicing plan" fails)". Optimize choosers for
:attr:`FollowUpRule.invoice_generator` and :attr:`Task.procedure` (which is
defined as :attr:`lino.modlib.linod.Runnable.procedure`.)
:attr:`Plan.invoicing_task` may now be empty. Optimize
:class:`lino.modlib.users.UserPlan` and :class:`lino.modlib.linod.Runnable`.
Fine-tune error reporting during :meth:`ApiElement.get`.


2025-02-10
==========

Fixed :ticket:`5917` (Clicking on the img rendered by file memo command doesn't
show the image). The demo data in :ref:`noi` now includes screenshots in ticket
descriptions in order to reproduce and verify this bug. New context manager
:meth:`ar.override_attrs <lino.api.core.Request.override_attrs>`.


2025-02-09
==========

Reviewed :func:`lino.utils.soup.truncate_comment` and updated
:ref:`book.topics.truncate` to fix :ticket:`5916`.

2025-02-05
==========

The :mod:`lino_xl.lib.ibanity` plugin is growing. New option :attr:`no_auto
<lino.modlib.checkdata.Checker.no_auto>` for data checkers.
:class:`lino_xl.lib.ibanity.SupplierChecker` sets this to `True` because we do
not want this checker to run automatically during :manage:`checkdata`. It should
run only when called manually because it requires :term:`Ibanity credentials`,
which are not available e.g. on GitLab.

2025-02-01
==========

Started working on the new plugin :mod:`lino_xl.lib.ibanity`. Work in progress.

2025-01-27
==========

Fixed :ticket:`5881` (Lino lets me add items to a registered invoice). Added a
new :meth:`lino.core.model.Model.disable_create` used to disable creating rows
on a model in a given :term:`action request` even when permission has been
given.


2025-01-24
==========

New setting :setting:`users.demo_password` so that the password of users on a
:term:`demo site` is no longer a hard-coded "1234".

2025-01-23
==========

The :mod:`lino_xl.lib.tim2lino` plugin was re-activated and the
:mod:`lino.utils.dbfreader` module migrated to Python 3.

The |sliders-h| button, which is used to show or hide the parameter panel of a
table, is now in the upper right corner and no longer in the toolbar.
