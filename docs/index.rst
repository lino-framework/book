.. sidebar:: **We need Pythonistas**

  - Grow your expertise in Python, Django, React, Git, Debian.
  - Work some hours per week in a small & fully remote team.
  - Contribute to a free software project.
  - Earn 200 € per month for doing what you would also do for free.

  | More information:
  | https://www.synodalsoft.net/jobs

This is the **Lino Developer Guide**, the :term:`documentation tree` for
:term:`developers <software developer>` of the :ref:`Lino framework <lf>`.

====================
Lino Developer Guide
====================


.. toctree::
   :maxdepth: 2

   welcome

   Get started </dev/getstarted>
   Dive into Lino </dev/diving>
   Contribute </contrib/index>

   /topics/index
   Reference </ref/index>
   /about/index
   /blog

.. toctree::
   :maxdepth: 2
   :hidden:

   /oldstuff
   copyright
   /dev/acquaintained
   /dev/index
   /money
