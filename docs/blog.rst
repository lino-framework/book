.. _blog:

====
News
====

Luc's developer blog is available at https://luc.lino-framework.org

Rumma & Ko Ltd have a blog at https://www.saffre-rumma.net/blog/

See also :ref:`lino.history`.
