=============
Testing pages
=============

This section contains independent pages that are meant as pure doctests rather
than to explain something. They are part of the test suite but have no value as
documentation.

.. toctree::
   :maxdepth: 1

   test_i18n
   core_utils
   dynamic
   e006
   gfks
   dashboard_sql
