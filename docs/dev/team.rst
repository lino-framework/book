.. _lino.dev.team:

===================
Working with others
===================


.. toctree::
   :maxdepth: 1

   contrib
   patch
   request_pull
   ci
   demo_projects
   invlib
