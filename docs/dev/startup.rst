.. _startup:

===================
When Lino starts up
===================

.. glossary::

  site startup

    The things that happen at the beginning of a :term:`Lino process`.

.. contents::
    :depth: 1
    :local:


Life cycle of a Lino process
============================

Here is what happens when a :term:`Lino process` wakes up.

.. glossary::

  Lino process

    A Python process on a given :term:`Lino site`.  This is either some
    :term:`django-admin command`, or a WSGI or ASGI process inside a web server.

Step by step:

- The :xfile:`manage.py` script causes the module specified by
  :envvar:`DJANGO_SETTINGS_MODULE` (your :xfile:`settings.py` module) to be
  imported. In this phase, everything in :mod:`lino.api.ad` is usable.

- Importing the :xfile:`settings.py` module will instantiate your
  :setting:`SITE`.

- When settings are ready, Django will load the :xfile:`models.py` modules.
  Everything in :mod:`lino.api.dd` is usable during this step.
  :mod:`lino.api.rt` may be imported but should not be accessed at global module
  level.

- When all :xfile:`models.py` modules are loaded, DJango has finished to start
  up and Lino may finally start up. This is what we call :term:`site startup`.
  The details of this phase are documented in `The Site startup phase`_ below.

- When site startup has finished, we finally enter the :term:`runtime` phase.
  Only now everything in :mod:`lino.api.rt` is usable.

There are **four major phases** in the life of a :term:`Lino process`:

.. glossary::

  application definition

    The first phase of the :term:`site startup` when Django **settings** are
    being loaded. We see this as a separate phase because Lino does some magics
    there.

  data definition

    The second phase of the :term:`site startup` when Django **models** are being
    loaded.

  site analysis

    The third phase of the :term:`site startup` when Django models have been
    loaded, and Lino analyzes them to fill its own data structures.

  runtime

    When the :term:`site startup` has finished and the actual process does what
    it is designed to do.

There are **three modules** in :mod:`lino.api` named after these phases:

- :mod:`lino.api.ad` is available during :term:`application definition`.

- :mod:`lino.api.dd` is available during :term:`data definition` and
  :term:`site analysis`.

- :mod:`lino.api.rt` is available when :term:`site analysis` has finised.



The Site startup phase
======================

.. currentmodule:: lino.core.site

The :term:`application definition` phase:

- :xfile:`manage.py` sets :envvar:`DJANGO_SETTINGS_MODULE` and calls
  :func:`django.core.management.execute_from_command_line`, which loads the
  :xfile:`settings.py` file.

The :class:`Site` class gets *instantiated* within the :xfile:`settings.py`
file. This instantiation does the following:

- Modify Django's :data:`django.utils.log.DEFAULT_LOGGING` dict.

- Call :meth:`Site.get_plugin_configs`

- Read the :xfile:`lino.ini` if it exists.

- Call :meth:`Site.get_plugin_modifiers` and :meth:`Site.get_installed_plugins`
  to build the list of plugins.

- Import each plugin (just the :file:`__init__.py` of the package).

- Set the :setting:`INSTALLED_APPS` setting and the
  :attr:`Site.installed_plugins` site attribute. The list of installed plugins
  is now known and won't change any more. :setting:`INSTALLED_APPS` is basically
  the same as :attr:`Site.installed_plugins`, except that the former is a list
  of module names while the latter is a list of :class:`Plugin` instances.

- Call each plugin's :meth:`on_plugins_loaded
  <lino.core.plugins.Plugin.on_plugins_loaded>` method.

Note that all the above-mentioned steps happen while the :xfile:`settings.py` is
*still being loaded*. This means for example that you cannot access the
:mod:`settings` module in your :meth:`get_plugin_configs
<Site.get_plugin_configs>` or :meth:`on_plugins_loaded
<lino.core.plugins.Plugin.on_plugins_loaded>` methods.

When Django has finished loading the :xfile:`settings.py` file, it starts
importing the :xfile:`models.py` module of each plugin. We call this the
:term:`data definition` phase.

When Django has fully populated its models registry (imported all
:xfile:`models.py` modules), it finds :class:`lino.AppConfig` and runs its
`ready() method
<https://docs.djangoproject.com/en/5.0/ref/applications/#django.apps.AppConfig.ready>`__,
which does nothing more and nothing less than run :meth:`Site.startup`.

The :term:`data definition` phase is done, we enter the :term:`site analysis`
phase. Which does the following:

- Instantiate the kernel. :attr:`settings.SITE.kernel <Site.kernel>` is now an
  instance of :class:`lino.core.kernel.Kernel` instead of `None`.

- Fill :attr:`Site.models`, an attrdict mapping plugin `app_label` to their
  curresponding :xfile:`models.py` module.

- Emit the :data:`lino.core.signals.pre_startup` signal

- Run :meth:`pre_site_startup <lino.core.plugins.Plugin.pre_site_startup>` of each
  plugin

- Run :meth:`Kernel.kernel_startup`, which does:

  - Register :meth:`Site.shutdown` to :mod:`atexit` and stop signal handler.

  - Install a :class:`DisableDeleteHandler
    <lino.core.ddh.DisableDeleteHandler>` for each model into
    :attr:`Model._lino_ddh`.

  - Install :class:`lino.core.model.Model` attributes and methods into raw
    Django Models that don't inherit from it.

  - Populate :attr:`Site.GFK_LIST`, a list of all :term:`generic foreign key`
    fields in the database.

  - Import :attr:`Site.user_types_module`

  - Analyze ForeignKeys and populate the :class:`DisableDeleteHandler
    <lino.core.ddh.DisableDeleteHandler>`.

  - Import :attr:`Site.workflows_module`

  - Run :meth:`lino.core.plugins.Plugin.before_analyze` on each plugin

  - Emit the :data:`lino.core.signals.pre_analyze` signal

  - Run :meth:`lino.core.site.Site.setup_actions`

  - Import :attr:`Site.custom_layouts_module` if defined.

  - Call :meth:`lino.core.model.Model.on_analyze` on every model.

  - Call :meth:`lino.core.model.Model.collect_virtual_fields` on every model.
    This attaches virtual fields to the model that declares them.

  - Run :meth:`lino.core.plugins.Plugin.before_actors_discover` on each plugin.

  - Discover and initialize actors

  - Emit the :data:`lino.core.signals.post_analyze` signal

  - Run :meth:`lino.core.actors.Actor.after_site_setup` on each actor

  - Emit the :data:`lino.core.signals.pre_ui_build` signal

  - Run :meth:`lino.core.plugins.Plugin.on_ui_init` on each plugin

- Run :attr:`Site.do_site_startup`

- Run :meth:`lino.core.plugins.Plugin.post_site_startup` on each plugin

- Emit the :data:`lino.core.signals.post_startup` signal
