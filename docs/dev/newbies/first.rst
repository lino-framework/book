.. _dev.first:

==========================
First Python sessions
==========================

This section is for people who have never talked with a computer. Welcome on
board!

.. contents::
  :local:


Your first date with Python
===========================

Open a terminal on your computer (on Linux hit :cmd:`Ctrl+Alt+T`, on Windows hit
the :kbd:`🪟 Win` key, type "cmd" and hit :kbd:`ENTER`).

In your terminal, type :cmd:`python` followed by :kbd:`ENTER`. It should say
something like::

  $ python
  Python 3.12.3 (main, Jan 17 2025, 18:03:48) [GCC 13.3.0] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>>

These three ">>>" are called the **prompt**. The prompt indicates that Python
waits for you to say something. Try to say::

  123+456

and then hit :kbd:`ENTER`. She will answer instantly::

  579

And again she asks you to say something. Talking with a computer is always like
this: you say something, she does what you want her to do, and then asks for
your next command. This way of talking with a computer is also called the
**command line**.

Next thing. Try the following::

  x=123

Python doesn't seem to answer anything, but actually  has stored a **value**
(123) in a **variable** (named "x"). You have now **defined** a **variable**.

Computers are very good in remembering things. From now on you can say::

  x+456

And she will tell you again 579. That's because she now knows that when you say
"x", you mean 123.

In case you aren't sure what "x" means, she can now remind you. Just type the
name of the variable::

  x

Now try to say **y** instead of **x** ::

  y+456

You'll get a longer answer::

  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  NameError: name 'y' is not defined

The first lines are a bit strange (imagine she got angry and therefore talked
rubbish), but the last line makes sense, does it? Yes, indeed you never told her
what "y" means, so of course she doesn't understand you. Python is nit-picky
regarding how to call things.

Okay, let's tell her what "y" means to you::

  y = 456

And then try again::

  x+y

Now she answers 567 because yes, when x is 123 and y is 456, then their sum is
579.

Python is very good in computing. Try the following ones as well::

  x*y
  x-y
  x/y

And a few more::

  x*x+5
  x*(x+5)
  x*x*x

You now know the four basic **operators** of the Python language: ``+``, ``-``,
``*`` and ``/``.

A last thing before we stop for this session::

  x=2
  y=3
  x*y

Yes, Python is not like a fundamental traditionalist, she is perfectly okay with
changing your mind about what the names **x** or **y** mean. You just
**redefine** them. That's by the way why they are called **variables**.

How to end a session with Python? Try to say "exit" and she will tell you. On my
Linux computer she says::

  >>> exit
  Use exit() or Ctrl-D (i.e. EOF) to exit

On Windows computers it's different. Don't ask me why...

.. rubric:: Keep in mind:

.. glossary::

  variable

    A name that you define so that it gets a "meaning" for Python. This
    "meaning" is usually called the :term:`value` of that variable.

  value

    The "meaning" or the "content" of a :term:`variable`. For example *123* or
    *456* are values.

  operator

    One of the symbols ``+``, ``-``, ``*`` and ``/``, which represent a
    mathematical operation on two values.

Working with text
=================

A :term:`variable` can hold other values than numbers. For example chunks of
text.  Today we'll play with these.
