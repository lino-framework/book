.. _lino.dev.design:

=================
Responsive design
=================

When designing your :term:`detail layouts <detail layout>`, you can help Lino to
render them correctly on any device.

Note that only the :term:`React front end` is reactive.  The users of a
:term:`Lino site` with an :term:`ExtJS front end` probably never use their data
from a mobile device.
