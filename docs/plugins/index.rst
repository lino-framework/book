.. _specs.xl:
.. _specs.core:
.. _dg.plugins:

================================
Plugins reference for developers
================================

This section assumes that you have read the :ref:`plugin reference for end user
<ug.plugins>`. See also :ref:`dg.topics.plugins`.


.. toctree::
   :maxdepth: 1

   about
   accounting
   /specs/addresses
   agenda
   ana
   /specs/appypod

   /specs/b2c
   /specs/beid
   bevat
   bevats
   blogs

   /specs/c2b
   cal
   /specs/calview
   changes
   checkdata
   /specs/clients
   /specs/coachings
   /specs/comments
   contacts
   /specs/countries
   courses
   /specs/cv

   /specs/dashboard
   /specs/deploy

   eevat
   /specs/events/index
   /specs/excerpts
   /specs/export_excel

   /specs/files
   finan

   google
   /specs/groups
   /specs/gfks

   /specs/healthcare
   help
   /specs/households
   /specs/humanlinks

   ibanity/index
   inbox
   invoicing

   /specs/jinja

   /specs/lino
   linod
   /specs/lists

   mastodon
   /specs/memo

   nicknames
   /specs/notes
   /specs/notify

   office
   /specs/orders

   peppol
   periods
   /specs/phones
   /specs/polls
   /specs/printing
   products
   /specs/properties
   publisher

   search
   summaries
   /specs/system
   /specs/ssin
   sepa
   sheets
   shopping
   smtpd
   sources
   storage
   subscriptions

   /specs/tickets
   /specs/tim2lino
   /specs/tinymce
   /specs/topics
   trading
   /specs/trends

   /specs/uploads
   users
   userstats

   vat
   votes
   warehouse
   /specs/weasyprint
   /specs/working

   /specs/xl
