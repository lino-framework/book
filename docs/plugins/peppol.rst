==========
``peppol``
==========

There is no longer a plugin called peppol. You probably want either
:doc:`/topics/peppol` or :doc:`/plugins/ibanity/index`.
