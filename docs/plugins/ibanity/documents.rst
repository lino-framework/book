.. doctest docs/plugins/ibanity/documents/.rst
.. _dg.plugins.ibanity.documents:

=================================
Send and receive Peppol documents
=================================

.. currentmodule:: lino_xl.lib.ibanity

With this usage scenario of the :mod:`lino_xl.lib.ibanity` plugin you can send
and receive your invoices and credit notes via the :term:`Peppol network`.

To activate this scenario, set the :data:`supplier_id` plugin setting to the
supplier id you received from your :term:`Ibanity hosting provider` who
registered your :term:`Peppol access`.


.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino_book.projects.cosi1.startup import *

The tests in this document are skipped unless you also have Ibanity credentials
installed. See :ref:`dg.plugins.ibanity.credentials` for details.

>>> if dd.plugins.ibanity.credentials is None:
...     pytest.skip('this doctest requires Ibanity credentials')

>>> translation.activate('en')
>>> outbound_model = dd.plugins.ibanity.outbound_model


Database models
===============

The ibanity plugin injects a checkbox field "is_outbound" into
:class:`accounting.Journal` and into :class:`contacts.Partner`. In the demo data
this field is checked (1) for journal SLS and (2) for some partners (a subset of
those with a vat_id).

.. currentmodule:: lino_xl.lib.contacts

.. class:: Partner
  :no-index:

  .. attribute:: is_outbound

    Whether sales invoices and credit notes to this partner should be sent via
    the :term:`Peppol network`.


.. currentmodule:: lino_xl.lib.ibanity



Tidy up after previous test runs:

>>> import shutil
>>> from lino.core.gfks import gfk2lookup
>>> def tidy_up():
...     for obj in ibanity.InboundDocument.objects.filter(voucher_id__isnull=False):
...         flt = gfk2lookup(uploads.Upload.owner, obj.voucher)
...         uploads.Upload.objects.filter(**flt).delete()
...         obj.voucher.delete()
...     ibanity.InboundDocument.objects.all().delete()
...     ibanity.OutboundInfo.objects.all().delete()
...     qs = outbound_model.objects.filter(state=accounting.VoucherStates.sent)
...     qs.update(state=accounting.VoucherStates.registered)
...     contacts.Partner.objects.exclude(peppol_id='').update(peppol_id='')
...     shutil.rmtree(dd.plugins.ibanity.inbox_dir, ignore_errors=True)

>>> tidy_up()

Suppliers management
====================

The `cosi1` demo sites has no suppliers management, this fictive company has
just received a `supplier_id` from their Lino provider.

>>> ar = rt.login("robin")

>>> dd.plugins.ibanity.with_suppliers
False

>>> ar.show(ibanity.OnboardingStates)
Traceback (most recent call last):
...
AttributeError: module 'lino_xl.lib.ibanity.models' has no attribute 'OnboardingStates'


>>> ar.show(ibanity.Suppliers)
Traceback (most recent call last):
...
AttributeError: module 'lino_xl.lib.ibanity.models' has no attribute 'Suppliers'


>>> dd.plugins.ibanity.supplier_id
'273c1bdf-6258-4484-b6fb-74363721d51f'


Outbound documents
==================

In the beginning our Outbox is empty:

>>> rt.show(ibanity.Outbox)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
No data to display


Fill the outbox with invoices to send:

>>> with ar.print_logger("DEBUG"):
...     rt.models.ibanity.collect_outbound(ar)  #doctest: +NORMALIZE_WHITESPACE
Collect outbound invoices into outbox
Scan 1 outbound journal(s): ['SLS']
Collect 5 new invoices into outbox
...

>>> rt.show(ibanity.Outbox)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
==================== ===================== ================ ============ ================= ==============
 Sales invoice        Partner               VAT regime       Entry date   Total excl. VAT   VAT
-------------------- --------------------- ---------------- ------------ ----------------- --------------
 SLS 1/2014           Bestbank              Subject to VAT   07/01/2014   2 999,85          629,97
 SLS 3/2014           Bäckerei Ausdemwald   Subject to VAT   09/01/2014   679,81            142,76
 SLS 4/2014           Bäckerei Mießen       Subject to VAT   10/01/2014   280,00            58,80
 SLS 5/2014           Bäckerei Schmitz      Subject to VAT   11/01/2014   535,00            112,35
 SLS 6/2014           Garage Mergelsberg    Subject to VAT   07/02/2014   1 110,16          203,87
 **Total (5 rows)**                                                       **5 604,82**      **1 147,75**
==================== ===================== ================ ============ ================= ==============
<BLANKLINE>



>>> ses = dd.plugins.ibanity.get_ibanity_session()

Send outbound documents:

>>> with ar.print_logger("DEBUG"):
...     ibanity.send_outbound(ses, ar)
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
Send outbound documents
Gonna send SLS 1/2014
Make .../media/xml/2014/SLS-2014-1.xml from SLS 1/2014 ...
Validate SLS-2014-1.xml against /home/luc/work/xl/lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch ...
Made .../media/xml/2014/SLS-2014-1.xml
Ibanity response {'attributes': {'createdAt': '2019-07-17T07:31:30.763402Z', 'status': 'created'}, 'id': '94884e80-cc4a-4583-bd4a-288095c7876f', 'relationships': {'supplier': {'data': {'id': '273c1bdf-6258-4484-b6fb-74363721d51f', 'type': 'supplier'}}}, 'type': 'peppolInvoice'}
Gonna send SLS 3/2014
Make .../media/xml/2014/SLS-2014-3.xml from SLS 3/2014 ...
Validate SLS-2014-3.xml against /home/luc/work/xl/lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch ...
Made .../media/xml/2014/SLS-2014-3.xml
Ibanity response {'attributes': {'createdAt': '2019-07-17T07:31:30.763402Z', 'status': 'created'}, 'id': '94884e80-cc4a-4583-bd4a-288095c7876f', 'relationships': {'supplier': {'data': {'id': '273c1bdf-6258-4484-b6fb-74363721d51f', 'type': 'supplier'}}}, 'type': 'peppolInvoice'}
Gonna send SLS 4/2014
Make .../media/xml/2014/SLS-2014-4.xml from SLS 4/2014 ...
Validate SLS-2014-4.xml against /home/luc/work/xl/lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch ...
Made .../media/xml/2014/SLS-2014-4.xml
Ibanity response {'attributes': {'createdAt': '2019-07-17T07:31:30.763402Z', 'status': 'created'}, 'id': '94884e80-cc4a-4583-bd4a-288095c7876f', 'relationships': {'supplier': {'data': {'id': '273c1bdf-6258-4484-b6fb-74363721d51f', 'type': 'supplier'}}}, 'type': 'peppolInvoice'}
Gonna send SLS 5/2014
Make .../media/xml/2014/SLS-2014-5.xml from SLS 5/2014 ...
Validate SLS-2014-5.xml against /home/luc/work/xl/lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch ...
Made .../media/xml/2014/SLS-2014-5.xml
Ibanity response {'attributes': {'createdAt': '2019-07-17T07:31:30.763402Z', 'status': 'created'}, 'id': '94884e80-cc4a-4583-bd4a-288095c7876f', 'relationships': {'supplier': {'data': {'id': '273c1bdf-6258-4484-b6fb-74363721d51f', 'type': 'supplier'}}}, 'type': 'peppolInvoice'}
Gonna send SLS 6/2014
Make .../media/xml/2014/SLS-2014-6.xml from SLS 6/2014 ...
Validate SLS-2014-6.xml against /home/luc/work/xl/lino_xl/lib/vat/XSD/PEPPOL-EN16931-UBL.sch ...
Made .../media/xml/2014/SLS-2014-6.xml
Ibanity response {'attributes': {'createdAt': '2019-07-17T07:31:30.763402Z', 'status': 'created'}, 'id': '94884e80-cc4a-4583-bd4a-288095c7876f', 'relationships': {'supplier': {'data': {'id': '273c1bdf-6258-4484-b6fb-74363721d51f', 'type': 'supplier'}}}, 'type': 'peppolInvoice'}

The Outbox table is now empty, and the invoices have moved to the Sent table.

>>> rt.show(ibanity.Outbox)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
No data to display

>>> rt.show(ibanity.Sent)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
=============== ===================== ============================ ========= =================
 Sales invoice   Partner               Created at                   State     Transmission ID
--------------- --------------------- ---------------------------- --------- -----------------
 SLS 1/2014      Bestbank              2019-07-17 07:31:30.763402   Created
 SLS 3/2014      Bäckerei Ausdemwald   2019-07-17 07:31:30.763402   Created
 SLS 4/2014      Bäckerei Mießen       2019-07-17 07:31:30.763402   Created
 SLS 5/2014      Bäckerei Schmitz      2019-07-17 07:31:30.763402   Created
 SLS 6/2014      Garage Mergelsberg    2019-07-17 07:31:30.763402   Created
=============== ===================== ============================ ========= =================
<BLANKLINE>

The status is "created" and they do not yet have any transmission ID. This will
change with the next synchronization step:

>>> with ar.print_logger("DEBUG"):
...     ibanity.followup_outbound(ses, ar)
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
Check outbound documents
SLS 1/2014 (ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5) state created becomes sent
SLS 3/2014 (ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5) state created becomes sent
SLS 4/2014 (ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5) state created becomes sent
SLS 5/2014 (ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5) state created becomes sent
SLS 6/2014 (ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5) state created becomes sent

>>> rt.show(ibanity.Sent)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
=============== ===================== ============================ ======= =======================================
 Sales invoice   Partner               Created at                   State   Transmission ID
--------------- --------------------- ---------------------------- ------- ---------------------------------------
 SLS 1/2014      Bestbank              2019-07-17 07:31:30.763402   Sent    ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5
 SLS 3/2014      Bäckerei Ausdemwald   2019-07-17 07:31:30.763402   Sent    ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5
 SLS 4/2014      Bäckerei Mießen       2019-07-17 07:31:30.763402   Sent    ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5
 SLS 5/2014      Bäckerei Schmitz      2019-07-17 07:31:30.763402   Sent    ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5
 SLS 6/2014      Garage Mergelsberg    2019-07-17 07:31:30.763402   Sent    ba6c26fa-f47c-4ef1-866b-71e4ef02f15a5
=============== ===================== ============================ ======= =======================================
<BLANKLINE>


Inbound documents
=================

In the beginning our Inbox is empty:

>>> rt.show(ibanity.Inbox)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
No data to display

Checking our inbox means that we ask whether we have received any new inbound
documents.

>>> with ar.print_logger("DEBUG"):
...     ibanity.check_inbox(ses, ar)
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
Check our inbox
We got a new document 431cb851-5bb2-4526-8149-5655d648292f

New documents are now visible in our database:

>>> rt.show(ibanity.Inbox)  #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
============================ ====================================== =======================================
 Created at                   DocumentId                             Transmission ID
---------------------------- -------------------------------------- ---------------------------------------
 ...                          431cb851-5bb2-4526-8149-5655d648292f   c038dbdc1-26ed-41bf-9ebf-37g3c4ceaa58
============================ ====================================== =======================================
<BLANKLINE>

Now Lino can download the detail of every single document.

>>> with ar.print_logger("DEBUG"):
...     ibanity.download_inbound(ses, ar)
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
Download 1 inbound documents
Download 431cb851-5bb2-4526-8149-5655d648292f
It's an invoice
Unknown Peppol ID 9931:100588749
Assign 9931:100588749 to partner Rumma & Ko OÜ because name matches
Supplier 9931:100588749 is Rumma & Ko OÜ
Lino ignores information in Metal chair 79.99
Lino ignores information in Website hosting 1MB/month 3.99
Lino ignores information in IT consultation & maintenance 30.00
Lino ignores information in <cbc:ID>SLS 3/2014</cbc:ID>
Store embedded file (PDF version) SLS-2014-3.pdf
Created INB 1/2014 from 431cb851-5bb2-4526-8149-5655d648292f


>>> print(dd.plugins.ibanity.inbox_dir)  #doctest: +ELLIPSIS
/.../projects/cosi1/media/ibanity_inbox

>>> for fn in dd.plugins.ibanity.inbox_dir.iterdir():
...     print(fn)
/.../projects/cosi1/media/ibanity_inbox/431cb851-5bb2-4526-8149-5655d648292f.xml




Choicelists
===========

.. class:: OutboundStates

>>> rt.show(ibanity.OutboundStates)
======= ============ ============
 value   name         text
------- ------------ ------------
 10      created      Created
 20      sending      Sending
 30      sent         Sent
 40      invalid      Invalid
 50      send_error   Send-Error
======= ============ ============
<BLANKLINE>


>>> rt.show(ibanity.OutboundErrors)
======= ========================= =========================
 value   name                      text
------- ------------------------- -------------------------
 010     malicious                 Malicious
 020     format                    Invalid format
 030     xsd                       Invalid XML
 040     schematron                Invalid Schematron
 050     identifiers               Invalid identifiers
 060     size                      Invalid size
 070     invalid_type              Invalid type
 080     customer_not_registered   Customer not registered
 090     unsupported               Type not supported
 100     access_point              Access Point issue
 110     unspecified               Unspecified error
======= ========================= =========================
<BLANKLINE>




At the end of this page we tidy up the database to avoid side effects in
following tests:

>>> tidy_up()
