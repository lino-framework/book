.. doctest docs/apps/noi/ibanity.rst
.. _noi.plugins.ibanity:

======================================
Managing Peppol accesses
======================================

.. currentmodule:: lino_xl.lib.ibanity

With this usage scenario of the :mod:`lino_xl.lib.ibanity` plugin you can manage
a list of :term:`Peppol accesses <Peppol access>`. To activate this scenario,
set the :data:`with_suppliers` plugin setting to `True`. This scenario requires
the :term:`site operator` to be an :term:`Ibanity hosting provider`.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino_book.projects.noi1r.startup import *

The tests in this document are skipped unless you have :term:`Ibanity
credentials` installed. See :ref:`dg.plugins.ibanity.credentials` for details.

>>> if not dd.plugins.ibanity.credentials:
...     pytest.skip('this doctest requires Ibanity credentials')


Clean up from (incomplete) previous test runs:

>>> def tidy_up():
...     ibanity.Supplier.objects.all().delete()
...     countries.Place.objects.filter(name="Leuven").delete()

>>> tidy_up()


.. currentmodule:: lino_xl.lib.ibanity

Database models
===============

.. class:: Supplier

  Django model used to represent an :term:`Ibanity supplier`


Data checkers
=============

.. class:: SuppliersListChecker

  Checks whether all suppliers that were registered by this :term:`Ibanity
  hosting provider` have a :class:`Supplier` row on this Lino site.

.. class:: SupplierChecker

  Synchronizes this :class:`Supplier` row with the data registered in the
  :term:`Ibanity API`.

Note that :class:`SuppliersListChecker` and :class:`SupplierChecker` are
**manual checkers**. We do not want these checkers to run automatically during
:manage:`checkdata`, only when called manually, because it requires Ibanity
credentials, which are not available e.g. on GitLab.


Onboarding states
=================

.. class:: OnboardingStates

  A choicelist with values for the :attr:`Supplier.onboarding_state` field.


>>> rt.show(ibanity.OnboardingStates)
======= ============ ============
 value   name         text
------- ------------ ------------
 10      draft        Draft
 20      created      Created
 30      approved     Approved
 40      rejected     Rejected
 50      onboarded    Onboarded
 60      offboarded   Offboarded
======= ============ ============
<BLANKLINE>

Suppliers
=========

>>> ar = rt.login("robin")
>>> rt.show(ibanity.Suppliers)
No data to display
>>> ibanity.SuppliersListChecker.update_unbound_problems(ar, fix=True)
- ibanity.SuppliersListChecker : (★) No entry for 273c1bdf-6258-4484-b6fb-74363721d51f.
(★) Unknown city Leuven ((in Belgium)
(★) Some fields need update: city, country, email, ibans, names, onboarding_state, phone, street, street_no, vat_id, zip_code
Found 0 and fixed 1 checkdata messages for ibanity.SuppliersListChecker (Check for missing or invalid suppliers).
>>> rt.show(ibanity.Suppliers)  #doctest: +ELLIPSIS
====================================== ======================= ============== ================== ==================================
 Supplier ID                            Names                   VAT id         Onboarding state   Last sync
-------------------------------------- ----------------------- -------------- ------------------ ----------------------------------
 273c1bdf-6258-4484-b6fb-74363721d51f   Company; Company S.A.   BE1234567890   Created            ...
====================================== ======================= ============== ================== ==================================
<BLANKLINE>


Clean up after this test run:

>>> tidy_up()
