.. doctest docs/projects/polly.rst
.. _dg.projects.polly:

===========================================
``polly`` : A little polls manager
===========================================

.. module:: lino_book.projects.polly

A little application for managing polls, explained in :ref:`polly`.

>>> from lino import startup
>>> startup('lino_book.projects.polly.settings.demo')
>>> from lino.api.doctest import *


>>> analyzer.show_dialog_actions()
- about.About.reset_password : Reset password
  (main) [visible for all]: **e-mail address** (email), **Username (optional)** (username), **New password** (new1), **New password again** (new2)
- about.About.sign_in : Sign in
  (main) [visible for all]:
  - (login_panel): **Username** (username), **Password** (password)
- about.About.verify_user : Verify
  (main) [visible for all]: **e-mail address** (email), **Verification code** (verification_code)
- polls.Polls.merge_row : Merge
  (main) [visible for all]: **into...** (merge_to), **Questions** (polls_Question), **Reason** (reason)
- users.AllUsers.change_password : Change password
  (main) [visible for all]: **Current password** (current), **New password** (new1), **New password again** (new2)
- users.AllUsers.merge_row : Merge
  (main) [visible for all]: **into...** (merge_to), **Reason** (reason)
- users.AllUsers.verify_me : Verify
  (main) [visible for all]: **Verification code** (verification_code)
<BLANKLINE>
