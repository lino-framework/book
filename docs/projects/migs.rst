.. doctest docs/projects/migs.rst
.. _book.projects.migs:

=========================================
``migs`` : testing migrations
=========================================

.. module:: lino_book.projects.migs

A :term:`demo project` for testing migrations.

The following tested docs use this project:

- :doc:`/specs/migrate`
