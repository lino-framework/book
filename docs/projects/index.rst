.. _book.specs.projects:

=============
Demo projects
=============

The :mod:`lino_book.projects` package contains a collection of :term:`demo
projects <demo project>`. This section lists some of them.


Usage examples of real-world :term:`Lino applications <Lino application>`:

.. toctree::
   :maxdepth: 1

   avanti1
   cms1
   cosi1
   cosi2
   cosi3
   cosi4
   cosi5
   noi1e
   noi1r
   noi2
   prima1
   tera1
   voga2
   voga3

Usage examples of minimal applications for educational purpose

.. toctree::
   :maxdepth: 1

   lets1
   chatter
   polly
   polls

Demo projects used mainly for testing

.. toctree::
   :maxdepth: 1

   migs
   /specs/projects/mti
   /specs/projects/nomti
   /specs/projects/belref
   /specs/projects/actors
   /specs/dumps
   /specs/projects/min
   /dev/watch
