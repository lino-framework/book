.. doctest docs/projects/prima1.rst
.. _dg.projects.prima1:

==================================================
``prima1`` : A Lino Prima
==================================================

An example of a :ref:`prima` using the :term:`React front end`.

This project has its own developer reference and is not covered by doctests of
the Developer Guide.

See :mod:`lino_prima.projects.prima1`
