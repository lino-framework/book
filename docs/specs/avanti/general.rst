.. doctest docs/specs/avanti/general.rst
.. _avanti.specs.general:

===============================
General overview of Lino Avanti
===============================

.. contents::
   :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_book.projects.avanti1.settings')
>>> from lino.api.doctest import *



Miscellaneous
=============


>>> dd.plugins.beid.holder_model
<class 'lino_avanti.lib.avanti.models.Client'>

The following checks whether the dashboard displays for user robin:

>>> url = "/"
>>> test_client.force_login(rt.login('robin').user)
>>> res = test_client.get(url, REMOTE_USER="robin")
>>> res.status_code
200
>>> soup = BeautifulSoup(res.content, "lxml")
>>> links = soup.find_all('a')
>>> len(links)
0

Status report
=============

Here is a text variant of Robin's dashboard.

TODO: The following test is skipped because the show_dashboard() function needs
a review. It can cause false failures and it doesn't cover all issues.


>>> show_dashboard('robin')
... #doctest: +REPORT_UDIFF +ELLIPSIS +NORMALIZE_WHITESPACE +SKIP
Quick links: [[Search](javascript:Lino.about.SiteSearch.grid.run\(null\))]
[[My settings](javascript:Lino.users.MySettings.detail.run\(null,{
"record_id": 1 }\) "Open a detail window on this record.")] [[My
Clients](javascript:Lino.avanti.MyClients.grid.run\(null\))] [[New
Client](javascript:Lino.avanti.MyClients.insert.run\(null\) "Open a dialog
window to insert a new Client.")] [[Read eID
card](javascript:Lino.list_action_handler\('/avanti/MyClients','find_by_beid','POST',Lino.beid_read_card_processor\)\(\)
"Find or create card holder from eID card")]
[[Refresh](javascript:Lino.viewport.refresh\(\);)]
<BLANKLINE>
Hi, Robin Rood! [There are 5 data problems assigned to
you.](javascript:Lino.checkdata.MyMessages.grid.run\(null,{ "base_params": {
}, "param_values": { "checker": null, "checkerHidden": null, "user": "Robin
Rood", "userHidden": 1 } }\))
<BLANKLINE>
This is a Lino demo site. ...
## My appointments
...
## My unconfirmed appointments
...
## Daily planner [⏏](javascript:Lino.calview.DailyPlanner.grid.run\(null\)
"Show this table in own window")
...
## Recent comments
...
## My Notification messages
...
## Status Report [⏏](javascript:Lino.courses.StatusReport.show.run\(null,{
...
### Language courses
<BLANKLINE>
<BLANKLINE>
