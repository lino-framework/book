.. _voga.specs:
.. _voga.tested:

=========================
Lino Voga Developer Guide
=========================

This is the developer documentation for :ref:`voga`.


.. toctree::
   :maxdepth: 1

   general
   usertypes
   voga
   presences
   holidays
   trading
   partners
   print_labels
   pupils
   db_roger
   plugins
