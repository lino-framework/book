.. _dg.voga.plugins:

====================
Plugins in Lino Voga
====================

.. toctree::
    :maxdepth: 1

    accounting
    courses
    cal
    invoicing
    checkdata
