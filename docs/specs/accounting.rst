.. _specs.accounting:

========================
Accounting stuff in Lino
========================

This topic guide explains everything a Lino :term:`application developer` needs
to know about accounting stuff.  It will never be finished.

    When designing an accounting package, the programmer operates as a
    mediator between people having different ideas: how it must
    operate, how its reports must appear, and how it must conform to
    the tax laws. By contrast, an operating system is not limited by
    outside appearances. When designing an operating system, the
    programmer seeks the simplest harmony between machine and
    ideas. This is why an operating system is easier to design.
    -- Tao of programming


The :ref:`xl` has several plugins that deal with accounting, and they are
integrated into the general framework. Here is our suggested order of reading
them:

- :doc:`/plugins/accounting`
- :doc:`/plugins/finan`
- :doc:`/plugins/products`
- :doc:`/plugins/trading`
- :doc:`/plugins/invoicing`
- :doc:`/plugins/ana`
- :doc:`/plugins/vat`
- :doc:`/plugins/bevat`
- :doc:`/plugins/eevat`
- :doc:`/plugins/bevats`
- :doc:`/plugins/sepa`
- :doc:`/plugins/sheets`
