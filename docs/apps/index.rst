.. _dg.apps:

=======================
Privileged applications
=======================

This section contains the developer docs of the :term:`privileged applications`.

See also the :ref:`User Guides <ug.apps>`.

.. toctree::
   :maxdepth: 1


   /apps/noi/index
   cosi/index
   /specs/avanti/index
   /specs/tera/index
   /specs/polly
   /specs/care
   /specs/voga/index
   cms/index
