.. _specs.cosi:
.. _cosi.tested:

=========================
Lino Così Developer Guide
=========================

This is the developer documentation for :ref:`cosi`.


.. toctree::
    :maxdepth: 1

    tim2lino
    invoicing
